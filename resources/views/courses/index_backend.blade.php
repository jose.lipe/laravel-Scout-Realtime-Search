@extends('layouts.app')

@section('content')
	<div class="container">

	<form action="{{ route('courses.index') }}" method="get">
		Search: <input type="text" name="str" value="{{ $str }}">
		<input type="submit" class="btn btn-primary" value="OK">
	</form>

	<h1>Courses</h1>
		<h4>Total: {{ count($courses) }}</h4>
		@foreach($courses as $course)
			<div class="col-md-3" style="border: 1px solid #ccc; margin: 10px; min-height: 400px;">
				<h3>{{ $course->name }}</h3>
				<p>{{ $course->description }} <br><br> By {{ $course->author }}</p>
			</div>
		@endforeach
	</div>
@endsection